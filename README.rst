This is a line processing tool that will remove a string
from your file. The target file and string is entered
as an argument on the command line at the time of execution.

usage: python eliminator.py <"target string"> <target file>

Author: John Merigliano | August 13, 2014
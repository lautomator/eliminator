#!/usr/bin/python

import sys
import re

# This is a line processing tool that will remove a string
# from your file. The target file and string is entered
# as an argument on the command line at the time of execution.
#
# usage: python eliminator.py <"target string"> <target file>
#
# Author: John Merigliano | August 13, 2014


# Remove the target and report results or errors.
def remove_target(target_string, target_file):
    '''
        Removes the target string from the file
        specified in the command line arguments.
    '''
    # Flag to proceed:
    ok = False

    try:
        # Open the file.
        fin = open(target_file, 'r')
        ok = True
    except Exception:
        print '[ERROR]: There is no such file-->', target_file

    # Proceed if the file is found.
    if ok:
        # Read the page.
        page = fin.read()

        # Compile the search term into a regexp to test
        # the existence of the search string.
        search_pattern = re.compile(target_string)

        # Remove the target, if found.
        if re.findall(search_pattern, page) != []:
            # Replace the bad code with ''
            new = page.replace(target_string, '')
            fin.close()
            # Report the results:
            print target_file, '-->[REMOVED]:', target_string

            # Overwrite the files and close.
            fout = open(target_file, 'w')
            fout.write(new)
            fout.close()
        else:
            print target_file, '-->[NO TARGET FOUND]:'


def main(args):
    # Execute the script within a file system.
    # Initiate the list to store all of the file names:
    try:
        target_string = args[0]
        target_file = args[1]
        remove_target(target_string, target_file)
    except Exception:
        print 'usage: python eliminator.py <"target string"> <target file>'


# Specify the target file(s).
args = sys.argv[1:]  # Exclude the name of the script.

if __name__ == '__main__':
    main(args)
